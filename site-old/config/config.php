<?php

/**
 * The config file is optional. It accepts a return array with config options
 * Note: Never include more than one return statement, all options go within this single return array
 * In this example, we set debugging to true, so that errors are displayed onscreen. 
 * This setting must be set to false in production.
 * All config options: https://getkirby.com/docs/reference/system/options
 */
return [
    'debug' => true,
    'cache' => [
        'pages' => [
            'active' => true
        ]
    ],
    'routes' => [[
        'pattern' => 'harvest',
        'method' => 'GET|POST',
        'action' => function () {
            $customHeader = $_SERVER['HTTP_X_REQUESTED_WITH'] ?? null;

            // Secure JSON output from direct access in production environment
            if (option('debug') === false && $customHeader !== 'fetch') {
                go(url('error'));
            }

            $curl = curl_init();

            curl_setopt_array($curl, array(
                CURLOPT_URL => "https://api.harvestapp.com/v2/users?is_active=true",
                CURLOPT_RETURNTRANSFER => true,
                CURLOPT_CUSTOMREQUEST => "GET",
                CURLOPT_HTTPHEADER => array(
                    "Authorization: Bearer 1065.pt.pOOQA3uzfcOU45RXMHtSazoKU11fgSdtpKz_IfvYAE83DDHCB7rlrbAqYuG9sgR6WuR_My5WtBvVtBGGSs078A",
                    "Harvest-Account-Id: 127871",
                    "User-Agent: PostmanRuntime/7.20.1"
                ),
            ));

            $response = curl_exec($curl);
            $err = curl_error($curl);

            curl_close($curl);

            if ($err) {
                return "cURL Error #:" . $err;
            } else {
                //$json = json_encode(json_decode($response), JSON_PRETTY_PRINT);
                $json = json_decode($response, true);
                $data = [];
                foreach ($json['users'] as $item) {
                    $data[] = [
                        'text' => $item['first_name'] . ' ' . $item['last_name'],
                        'value' => $item['id']
                    ];
                }
                //return ['Users' => $json['users']];
                //return json_encode($data, JSON_PRETTY_PRINT);
                //return new Response(json_encode($data), 'application/json');
                $test = '{
  "Companies": [
    {"name": "Apple"},
    {"name": "Intel"},
    {"name": "Microsoft"}
  ]
}';
                return new Response($test, 'application/json');
            }
        }
    ]]
];
