panel.plugin("hello/world", {
  fields: {
    hello: {
      extends: "k-text-string",
      props: {
        message: String
      },
      template: "<p>{{ message }}</p>"
    }
  }
});
